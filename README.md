# Foundry VTT 核心系统正體中文化(自用)


## 安装方法
- 使用連結手動安裝，清單檔位址 [https://gitlab.com/yellowin/foundry_cht_v7/-/raw/master/module.json](https://gitlab.com/yellowin/foundry_cht_v7/-/raw/master/module.json)

使用該中文化模組，需要禁用或卸載其他核心系統中文化模組（遊戲系統的中文化模組不影響）。

## 鳴謝
**感謝貓斯基及其他原中文化版本譯者**
**CREDIT https://github.com/fvtt-cn/foundry_chn**